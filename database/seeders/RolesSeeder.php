<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'role_name' => 'Admin',
                'created_at' => now()
            ],
            [
                'role_name' => 'User',
                'created_at' => now()
            ],
        ];

        Role::insert($roles);
    }
}
