<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_number');
            $table->integer('user_id');
            $table->integer('subscription_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->float('amount');
            $table->string('payment_method')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0: inprogress/pending', '1: success', '2: failed', '3: expired');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
