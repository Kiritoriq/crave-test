<?php

use App\Http\Controllers\admin\Auth\LoginController;
use App\Http\Controllers\admin\SubscriptionPlanController;
use App\Http\Controllers\admin\TemplateController;
use App\Http\Controllers\admin\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => 'portal',
    'as' => 'portal.',
    'middleware' => 'web'
], function() {
    Route::get('/', function() {
        return view('portal.pages.home.index');
    })->name('home');
});

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'middleware' => ['web']
], function() {
    // Auth Routes
    Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('/login', [LoginController::class, 'login'])->name('login.action');
    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

    // Must 'Auth' Routes
    Route::group([
        'middleware' => ['auth']
    ], function() {
        // Dashboard routes
        Route::get('/dashboard', function() {
            return view('admin.pages.dashboard.index');
        })->name('dashboard');

        // User's Routes
        Route::get('/users', [UserController::class, 'index'])->name('users');
        Route::get('/users/create', [UserController::class, 'create'])->name('users.create');
        Route::post('/users/create', [UserController::class, 'store'])->name('users.store');
        Route::get('/users/{id}/edit', [UserController::class, 'edit'])->name('users.edit');
        Route::post('/users/{id}/edit', [UserController::class, 'update'])->name('users.update');
        Route::delete('/users/{id}/delete', [UserController::class, 'destroy'])->name('users.delete');

        // Template's Routes
        Route::get('/templates', [TemplateController::class, 'index'])->name('templates');
        Route::get('/templates/create', [TemplateController::class, 'create'])->name('templates.create');
        Route::post('/templates/create', [TemplateController::class, 'store'])->name('templates.store');
        Route::get('/templates/{id}/edit', [TemplateController::class, 'edit'])->name('templates.edit');
        Route::post('/templates/{id}/edit', [TemplateController::class, 'update'])->name('templates.update');
        Route::delete('/templates/{id}/delete', [TemplateController::class, 'destroy'])->name('templates.delete');

        // Subscription Plan's Routes
        Route::get('/subscription_plan', [SubscriptionPlanController::class, 'index'])->name('subscription_plan');
        Route::get('/subscription_plan/create', [SubscriptionPlanController::class, 'create'])->name('subscription_plan.create');
        Route::post('/subscription_plan/create', [SubscriptionPlanController::class, 'store'])->name('subscription_plan.store');
        Route::get('/subscription_plan/{id}/edit', [SubscriptionPlanController::class, 'edit'])->name('subscription_plan.edit');
        Route::post('/subscription_plan/{id}/edit', [SubscriptionPlanController::class, 'update'])->name('subscription_plan.update');
        Route::delete('/subscription_plan/{id}/delete', [SubscriptionPlanController::class, 'destroy'])->name('subscription_plan.delete');
    });
});

Route::get('/admin', function() {
    return redirect('/admin/dashboard');
});

Route::get('/', function () {
    return view('portal.pages.home.index');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
