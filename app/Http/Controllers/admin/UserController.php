<?php

namespace App\Http\Controllers\admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Src\Actions\admin\Users\{GetUserIndexAction, StoreUserAction, GetUserByIdAction, UpdateUserAction, DeleteUserAction};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $users = (new GetUserIndexAction($request->all()))->execute();
            $roles = Role::all();

            return view('admin.pages.users.index', [
                'users' => $users,
                'roles' => $roles
            ]);
        } catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.users.create', [
            'roles' => Role::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'role' => 'required',
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'type' => 'validate',
                'error' => ucwords(implode(', ', $validator->errors()->all()))
            ]);
        }

        try {
            $user = (new StoreUserAction($request->all()))->execute();

            return Helper::jsonSuccessResponse('User created!', $user);
        } catch(\Exception $e) {
            return Helper::jsonFailedResponse('', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = (new GetUserByIdAction(decrypt($id)))->execute();

        return view('admin.pages.users.edit', [
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'role' => 'required',
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'type' => 'validate',
                'error' => ucwords(implode(', ', $validator->errors()->all()))
            ]);
        }

        try {
            $user = (new UpdateUserAction($request->all(), decrypt($id)))->execute();

            return Helper::jsonSuccessResponse('User updated!', $user);
        } catch(\Exception $e) {
            return Helper::jsonFailedResponse('', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = (new DeleteUserAction(decrypt($id)))->execute();

            return Helper::jsonSuccessResponse('User deleted!', $user);
        } catch(\Exception $e) {
            return Helper::jsonFailedResponse('', $e->getMessage());
        }
    }
}
