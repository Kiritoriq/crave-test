<?php

namespace App\Http\Controllers\admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Src\Actions\admin\SubscriptionPlan\{GetSubscriptionIndexAction, GetSubscriptionByIdAction, StoreSubscriptionAction, UpdateSubscriptionAction, DeleteSubscriptionAction};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubscriptionPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $subscriptions = (new GetSubscriptionIndexAction($request->all()))->execute();

            return view('admin.pages.subscription_plan.index', [
                'subscriptions' => $subscriptions,
            ]);
        } catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.subscription_plan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'benefit' => 'required',
            'duration' => 'required|numeric|min:1'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'type' => 'validate',
                'error' => ucwords(implode(', ', $validator->errors()->all()))
            ]);
        }

        try {
            $user = (new StoreSubscriptionAction($request->all()))->execute();

            return Helper::jsonSuccessResponse('Subscription Plan created!', $user);
        } catch(\Exception $e) {
            return Helper::jsonFailedResponse('', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscription = (new GetSubscriptionByIdAction(decrypt($id)))->execute();

        return view('admin.pages.subscription_plan.edit', [
            'subscription' => $subscription
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'benefit' => 'required',
            'duration' => 'required|numeric|min:1'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'type' => 'validate',
                'error' => ucwords(implode(', ', $validator->errors()->all()))
            ]);
        }

        try {
            $user = (new UpdateSubscriptionAction($request->all(), decrypt($id)))->execute();

            return Helper::jsonSuccessResponse('Subscription Plan created!', $user);
        } catch(\Exception $e) {
            return Helper::jsonFailedResponse('', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $subscription = (new DeleteSubscriptionAction(decrypt($id)))->execute();

            return Helper::jsonSuccessResponse('Subscription Plan deleted!', $subscription);
        } catch(\Exception $e) {
            return Helper::jsonFailedResponse('', $e->getMessage());
        }
    }
}
