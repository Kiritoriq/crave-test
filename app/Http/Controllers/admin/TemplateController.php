<?php

namespace App\Http\Controllers\admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Src\Actions\admin\Templates\{GetTemplateIndexAction, GetTemplateByIdAction, StoreTemplateAction, UpdateTemplateAction, DeleteTemplateAction};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $templates = (new GetTemplateIndexAction($request->all()))->execute();

            return view('admin.pages.templates.index', [
                'templates' => $templates,
            ]);
        } catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.templates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'is_premium' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'type' => 'validate',
                'error' => ucwords(implode(', ', $validator->errors()->all()))
            ]);
        }

        try {
            $template = (new StoreTemplateAction($request->all()))->execute();

            return Helper::jsonSuccessResponse('Template created!', $template);
        } catch(\Exception $e) {
            return Helper::jsonFailedResponse('', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template = (new GetTemplateByIdAction(decrypt($id)))->execute();

        return view('admin.pages.templates.edit', [
            'template' => $template
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'is_premium' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'type' => 'validate',
                'error' => ucwords(implode(', ', $validator->errors()->all()))
            ]);
        }

        try {
            $template = (new UpdateTemplateAction($request->all(), decrypt($id)))->execute();

            return Helper::jsonSuccessResponse('Template updated!', $template);
        } catch(\Exception $e) {
            return Helper::jsonFailedResponse('', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $template = (new DeleteTemplateAction(decrypt($id)))->execute();

            return Helper::jsonSuccessResponse('Template deleted!', $template);
        } catch(\Exception $e) {
            return Helper::jsonFailedResponse('', $e->getMessage());
        }
    }
}
