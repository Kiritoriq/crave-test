<?php

namespace App\Src\Actions\admin\Users;

use App\Models\User;

class DeleteUserAction
{
    /**
     * @var id
     */
    private int $id;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function execute()
    {
        return $this->delete();
    }

    public function delete()
    {
        return User::findOrFail($this->id)->delete();
    }
}