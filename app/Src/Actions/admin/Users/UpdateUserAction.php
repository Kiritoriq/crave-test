<?php

namespace App\Src\Actions\admin\Users;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UpdateUserAction
{
    /**
     * @var int
     */
    private int $id;

    /**
     * @var array
     */
    private $data;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(array $data, int $id)
    {
        $this->data = $data;
        $this->id = $id;
    }

    public function execute()
    {
        return $this->update();
    }

    public function update()
    {
        if(blank($this->data['password'])) {
            return User::findOrFail($this->id)
                ->update([
                    'role_id' => $this->data['role'],
                    'name' => $this->data['name'],
                    'email' => $this->data['email'],
                    'updated_at' => now()
                ]);
        } else {
            return User::findOrFail($this->id)
                ->update([
                    'role_id' => $this->data['role'],
                    'name' => $this->data['name'],
                    'email' => $this->data['email'],
                    'password' => Hash::make($this->data['password']),
                    'updated_at' => now()
                ]);
        }
    }
}