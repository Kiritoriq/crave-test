<?php

namespace App\Src\Actions\admin\Users;

use App\Models\User;

class GetUserByIdAction
{
    /**
     * @var id
     */
    private int $id;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function execute()
    {
        return $this->getUserById();
    }

    public function getUserById()
    {
        return User::where('id', $this->id)->first();
    }
}