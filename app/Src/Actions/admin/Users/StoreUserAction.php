<?php

namespace App\Src\Actions\admin\Users;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class StoreUserAction
{
    /**
     * @var array
     */
    private $data;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function execute()
    {
        return $this->store();
    }

    public function store()
    {
        return User::create([
            'role_id' => $this->data['role'],
            'name' => $this->data['name'],
            'email' => $this->data['email'],
            'password' => Hash::make($this->data['password']),
            'created_at' => now()
        ]);
    }
}