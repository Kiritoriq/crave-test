<?php

namespace App\Src\Actions\admin\Users;

use App\Models\User;

class GetUserIndexAction
{

    /**
     * @var array
     */
    private $request;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(array $request)
    {
        $this->request = $request;
    }

    public function execute()
    {
        return $this->getUsers();
    }

    public function getUsers()
    {
        $per_page = env('PER_PAGE', 10);
        $role_id = (isset($this->request['role_id']) ? $this->request['role_id'] : null);
        $search = (isset($this->request['search']) ? $this->request['search'] : null);
        return User::with('role')
            ->whereHas('role', function($q) use ($role_id) {
                if(! blank($role_id)) {
                    $q->where('id', $role_id);
                }
            })
            ->when(! blank($search), function($q) use ($search) {
                $q->where('name', 'ilike', '%' . $search . '%');
                $q->orWhere('email', 'ilike', '%' . $search . '%');
            })
            ->paginate($per_page);
    }
}