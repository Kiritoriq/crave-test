<?php

namespace App\Src\Actions\admin\SubscriptionPlan;

use App\Models\SubscriptionPlan;

class UpdateSubscriptionAction
{
    /**
     * @var int
     */
    private int $id;

    /**
     * @var array
     */
    private $data;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(array $data, int $id)
    {
        $this->data = $data;
        $this->id = $id;
    }

    public function execute()
    {
        return $this->update();
    }

    public function update()
    {
        return SubscriptionPlan::findOrFail($this->id)
            ->update([
                'name' => $this->data['name'],
                'price' => intval($this->data['price']),
                'benefit' => $this->data['benefit'],
                'duration' => $this->data['duration'],
                'updated_at' => now()
            ]);
    }
}