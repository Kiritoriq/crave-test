<?php

namespace App\Src\Actions\admin\SubscriptionPlan;

use App\Models\SubscriptionPlan;

class StoreSubscriptionAction
{
    /**
     * @var array
     */
    private $data;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function execute()
    {
        return $this->store();
    }

    public function store()
    {
        return SubscriptionPlan::create([
            'name' => $this->data['name'],
            'price' => intval($this->data['price']),
            'benefit' => $this->data['benefit'],
            'duration' => $this->data['duration'],
            'created_at' => now()
        ]);
    }
}