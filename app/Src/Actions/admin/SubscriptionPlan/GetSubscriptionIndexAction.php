<?php

namespace App\Src\Actions\admin\SubscriptionPlan;

use App\Models\SubscriptionPlan;

class GetSubscriptionIndexAction
{
    /**
     * @var array
     */
    private $request;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(array $request)
    {
        $this->request = $request;
    }

    public function execute()
    {
        return $this->getSubscriptionPlans();
    }

    public function getSubscriptionPlans()
    {
        $per_page = env('PER_PAGE', 10);
        $search = (isset($this->request['search']) ? $this->request['search'] : null);
        return SubscriptionPlan::when(! blank($search), function($q) use ($search) {
                $q->where('name', 'ilike', '%' . $search . '%');
            })
            ->paginate($per_page);
    }
}