<?php

namespace App\Src\Actions\admin\SubscriptionPlan;

use App\Models\SubscriptionPlan;

class GetSubscriptionByIdAction
{
    /**
     * @var id
     */
    private int $id;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function execute()
    {
        return $this->getSubscriptionById();
    }

    public function getSubscriptionById()
    {
        return SubscriptionPlan::where('id', $this->id)->first();
    }
}