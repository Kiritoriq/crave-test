<?php

namespace App\Src\Actions\admin\SubscriptionPlan;

use App\Models\SubscriptionPlan;

class DeleteSubscriptionAction
{
    /**
     * @var id
     */
    private int $id;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function execute()
    {
        return $this->delete();
    }

    public function delete()
    {
        return SubscriptionPlan::findOrFail($this->id)->delete();
    }
}