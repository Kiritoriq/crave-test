<?php

namespace App\Src\Actions\admin\Templates;

use App\Models\EventTemplate;

class GetTemplateByIdAction
{
    /**
     * @var id
     */
    private int $id;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function execute()
    {
        return $this->getTemplateById();
    }

    public function getTemplateById()
    {
        return EventTemplate::where('id', $this->id)->first();
    }
}