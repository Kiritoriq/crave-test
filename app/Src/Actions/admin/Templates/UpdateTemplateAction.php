<?php

namespace App\Src\Actions\admin\Templates;

use App\Models\EventTemplate;
use Illuminate\Support\Facades\Auth;

class UpdateTemplateAction
{
    /**
     * @var int
     */
    private int $id;

    /**
     * @var array
     */
    private $data;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(array $data, int $id)
    {
        $this->data = $data;
        $this->id = $id;
    }

    public function execute()
    {
        return $this->update();
    }

    public function update()
    {
        return EventTemplate::findOrFail($this->id)
            ->update([
                'category_id' => NULL,
                'content' => $this->data['content'],
                'is_premium' => $this->data['is_premium'],
                'updated_by' => Auth::user()->id,
                'updated_at' => now()
            ]);
    }
}