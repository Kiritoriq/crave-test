<?php

namespace App\Src\Actions\admin\Templates;

use App\Models\EventTemplate;
use Illuminate\Support\Facades\Auth;

class StoreTemplateAction
{
    /**
     * @var array
     */
    private $data;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function execute()
    {
        return $this->store();
    }

    public function store()
    {
        return EventTemplate::create([
            'category_id' => NULL,
            'content' => $this->data['content'],
            'is_premium' => $this->data['is_premium'],
            'created_by' => Auth::user()->id,
            'created_at' => now()
        ]);
    }
}