<?php

namespace App\Src\Actions\admin\Templates;

use App\Models\EventTemplate;

class DeleteTemplateAction
{
    /**
     * @var id
     */
    private int $id;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function execute()
    {
        return $this->delete();
    }

    public function delete()
    {
        return EventTemplate::findOrFail($this->id)->delete();
    }
}