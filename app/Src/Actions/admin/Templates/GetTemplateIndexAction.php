<?php

namespace App\Src\Actions\admin\Templates;

use App\Models\EventTemplate;

class GetTemplateIndexAction
{
    /**
     * @var array
     */
    private $request;

    /**
     * New class instance
     * 
     * @return void
     */
    public function __construct(array $request)
    {
        $this->request = $request;
    }

    public function execute()
    {
        return $this->getTemplates();
    }

    public function getTemplates()
    {
        $per_page = env('PER_PAGE', 10);
        $search = (isset($this->request['search']) ? $this->request['search'] : null);
        return EventTemplate::when(! blank($search), function($q) use ($search) {
                $q->where('name', 'ilike', '%' . $search . '%');
            })
            ->paginate($per_page);
    }
}