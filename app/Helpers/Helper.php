<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;

class Helper
{
    public static function jsonSuccessResponse(string $msg='', Model|Collection|array|bool $data): JsonResponse
    {
        return response()->json([
            'status' => 'success',
            'msg' => (blank($msg) ? 'Success!' : $msg),
            'data' => (blank($data) ? array() : $data)
        ]);
    }

    public static function jsonFailedResponse(string $msg='', string $error): JsonResponse
    {
        return response()->json([
            'status' => 'success',
            'msg' => (blank($msg) ? 'An Error Occured!' : $msg),
            'error' => (blank($error) ? array() : $error)
        ]);
    }
}