@extends('portal.layouts.app')

@section('title', env('APP_NAME') . ' | Home')

@section('content')
{{-- Banner --}}
@include('portal.pages.home.includes.banner')
{{-- Pricing --}}
@include('portal.pages.home.includes.pricing')
@endsection

{{-- Styles Section --}}
@section('styles')
@endsection

{{-- Scripts Section --}}
@section('scripts')
@endsection