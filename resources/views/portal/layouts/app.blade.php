<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', env('APP_NAME', 'Laravel'))</title>
    <!-- bootstrap.min css -->
    <link rel="stylesheet" href="{{ asset('promodise/plugins/bootstrap/css/bootstrap.css') }}">
    <!-- Icofont Css -->
    <link rel="stylesheet" href="{{ asset('promodise/plugins/fontawesome/css/all.css') }}">
    <!-- animate.css -->
    <link rel="stylesheet" href="{{ asset('promodise/plugins/animate-css/animate.css') }}">
    <!-- Icofont -->
    <link rel="stylesheet" href="{{ asset('promodise/plugins/icofont/icofont.css') }}">

    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="{{ asset('promodise/css/style.css') }}">
    @yield('styles')
</head>
<body data-spy="scroll" data-target=".fixed-top">
    @include('portal.layouts.base.navbar')

    @yield('content')

    @include('portal.layouts.base.footer')
    
    <!-- Main jQuery -->
    <script src="{{ asset('promodise/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4.3.1 -->
    <script src="{{ asset('promodise/plugins/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('promodise/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
   <!-- Woow animtaion -->
    <script src="{{ asset('promodise/plugins/counterup/wow.min.js') }}"></script>
    <script src="{{ asset('promodise/plugins/counterup/jquery.easing.1.3.js') }}"></script>
     <!-- Counterup -->
    <script src="{{ asset('promodise/plugins/counterup/jquery.waypoints.js') }}"></script>
    <script src="{{ asset('promodise/plugins/counterup/jquery.counterup.min.js') }}"></script>

    <!-- Google Map -->
    <script src="{{ asset('promodise/plugins/google-map/gmap3.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkeLMlsiwzp6b3Gnaxd86lvakimwGA6UA&callback=initMap"></script>   
    <!-- Contact Form -->
    <script src="{{ asset('promodise/plugins/jquery/contact.js') }}"></script>
    <script src="{{ asset('promodise/js/custom.js') }}"></script>
    @yield('scripts')
</body>
</html>