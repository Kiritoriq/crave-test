<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', env('APP_NAME', 'Laravel'))</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('quixlab/images/favicon.png') }}">
    <!-- Pignose Calender -->
    <link href="{{ asset('quixlab/plugins/pg-calendar/css/pignose.calendar.min.css') }}" rel="stylesheet">
    {{-- Font Awesome --}}
    <link rel="stylesheet" href="{{ asset('quixlab/icons/font-awesome/css/font-awesome.min.css') }}">
    <!-- Custom Stylesheet -->
    <link href="{{ asset('quixlab/plugins/sweetalert/css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('quixlab/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/fancybox/jquery.fancybox.min.css') }}">
    @yield('styles')
</head>
<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    {{-- Main Wrapper Start --}}
    <div id="main-wrapper">
        @include('admin.layouts.base.header')
        @include('admin.layouts.base.sidebar')

        <div class="content-body">
            @yield('content')
        </div>

        @include('admin.layouts.base.footer')
    </div>
    {{-- Main Wrapper End --}}

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="{{ asset('quixlab/plugins/common/common.min.js') }}"></script>
    <script src="{{ asset('quixlab/js/custom.min.js') }}"></script>
    <script src="{{ asset('quixlab/js/settings.js') }}"></script>
    <script src="{{ asset('quixlab/js/gleek.js') }}"></script>
    <script src="{{ asset('quixlab/js/styleSwitcher.js') }}"></script>

    <script src="{{ asset('quixlab/plugins/sweetalert/js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('plugins/fancybox/jquery.fancybox.min.js') }}"></script>
    <script>
        function swalLoading()
        {
            return swal({
                    title: '',
                    text: `<i class="fa fa-circle-o-notch fa-2x fa-spin"></i>`,
                    showConfirmButton: false,
                    html: true
                })
        }
    </script>
    @yield('scripts')
</body>
</html>