<div class="container-fluid mt-3" style="max-width: 800px">
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('admin.users.update', encrypt($user->id)) }}" method="POST" id="user-form">
                @csrf
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Edit Admin User</h3>
                    </div>
                </div>
                <div class="card-body">
                    <input type="hidden" name="role" value="1">
                        <div class="basic-form">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control input-default" name="name" value="{{ $user->name }}" id="name" placeholder="Fill it with Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control input-default" name="email" value={{ $user->email }} id="email" placeholder="Fill it with Email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control input-default" name="password" id="password" placeholder="Fill it with Password">
                                    <small>If you don't want change the password, just leave the password field blank.</small>
                                </div>
                            </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-5" style="float: left">
                            
                        </div>
                        <div class="col-md-7">
                            <div style="float: right">
                                <button type="submit" class="btn btn-success">
                                    <i class="icon-pencil"></i> Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- #/ container -->
<script>
    $(document).ready(function() {
        $('#user-form').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: $(this).serialize(),
                success: function(response) {
                    if(response.status === 'success') {
                        swal({
                            title: 'Success!',
                            text: response.msg,
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }, () => {
                            window.location.reload();
                        })
                    } else {
                        swal({
                            title: response.msg,
                            text: response.error,
                            type: 'error',
                        })
                    }
                }
            })
        })
    })
</script>