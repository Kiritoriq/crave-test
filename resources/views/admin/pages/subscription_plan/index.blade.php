@extends('admin.layouts.app')

@section('title', env('APP_NAME') . ' | Subscription Plans')

@section('content')
<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Home</a></li>
        </ol>
    </div>
</div>
<div class="container-fluid mt-3">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Subscription Plan Management</h3>
                    </div>
                </div>
                <div class="card-body">
                    <form action="" method="GET">
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label>&nbsp;</label>
                                <a href="javascript:void(0)" data-href="{{ route('admin.subscription_plan.create') }}" class="btn btn-success btn-block btnAdd">
                                    <i class="icon-plus"></i> Create subscription
                                </a>
                            </div>
                            <div class="form-group col-md-3">
                                
                            </div>
                            <div class="form-group col-md-3">
                                <label>Search Field</label>
                                <input type="text" class="form-control input-default" name="search" id="search" value="{{ Request()->search }}" placeholder="Search by subscription's name or email">
                            </div>
                            <div class="form-group col-md-3">
                                <label>&nbsp;</label>
                                <button class="btn btn-primary btn-block" type="submit" href="{{ url()->current() }}?search={{ Request()->search }}">
                                    <i class="icon-magnifier"></i> Search
                                </button>
                            </div>
                        </div>
                    </form>
                    {{-- Table Index subscriptions --}}
                    <div class="table-responsive">
                        <table class="table table-striped table-hover" id="subscriptions-table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Name</td>
                                    <td>Price</td>
                                    <td>Benefit</td>
                                    <td>Duration</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($subscriptions) > 0)
                                    @foreach($subscriptions as $index => $subscription)
                                        <tr>
                                            <td>{!! ($index+1)+((Request::input('page') != 0 ? (Request::input('page')-1) : Request::input('page'))*1) !!}</td>
                                            <td>{{ $subscription->name }}</td>
                                            <td>{{ number_format($subscription->price) }}</td>
                                            <td>{{ $subscription->benefit }}</td>
                                            <td>{{ $subscription->duration }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="edit-data" data-href="{{ route('admin.subscription_plan.edit', encrypt($subscription->id)) }}" title="Edit subscription">
                                                    <i class="icon-note text-info"></i>
                                                </a>
                                                <a href="javascript:void(0)" class="delete-data" title="Delete subscription" data-href="{{ route('admin.subscription_plan.delete', encrypt($subscription->id)) }}">
                                                    <i class="icon-trash text-danger"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" style="text-align: center">
                                            <i>Data Not Found</i>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    {{-- End of Table Index subscriptions --}}
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-5" style="float: left">
                            <small>Showing {{ $subscriptions->count() }} from {{ $subscriptions->total() }} data</small>
                        </div>
                        <div class="col-md-7">
                            <div style="float: right">
                                {!! $subscriptions->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/ container -->
@endsection

{{-- Styles Section --}}
@section('styles')
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    function deletesubscription(url)
    {
        $.ajax({
                url: url,
                type: 'DELETE',
                data: {
                    _token: '{{ csrf_token() }}'
                },
                success: function(response) {
                    if(response.status === 'success') {
                        swal({
                            title: 'Success!',
                            text: response.msg,
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }, () => {
                            window.location.reload();
                        })
                    } else {
                        swal({
                            title: response.msg,
                            text: response.error,
                            type: 'error',
                        })
                    }
                }
            })
    }

    $(document).ready(function() {
        $('.btnAdd').click(function(e) {
            e.preventDefault()
            swalLoading()
            $.ajax({
                url: $(this).attr('data-href'),
                type: 'GET',
                success: function(response) {
                    swal.close()
                    $.fancybox.open([
                        {
                            src: response,
                            type: 'html'
                        }
                    ])
                }
            })
        })

        $('.edit-data').on('click', function(e) {
            e.preventDefault()
            swalLoading()
            $.ajax({
                url: $(this).attr('data-href'),
                type: 'GET',
                success: function(response) {
                    swal.close()
                    $.fancybox.open([
                        {
                            src: response,
                            type: 'html'
                        }
                    ])
                }
            })
        })

        $('.delete-data').on('click', function(e) {
            e.preventDefault()
            let url = $(this).attr('data-href');
            swal({
                type: 'warning',
                title: 'Are you sure want to delete data?',
                text: 'Once deleted, data cannot be restore',
                showCancelButton: true
            }, (result) => {
                if(result) {
                    deletesubscription(url)
                }
            })
        })
    })
</script>
@endsection